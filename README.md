## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Run Migration
```
$ npm run migration:run
```

## Swagger Access

Url : localhost:3000/api

## User Authentication

username : aniruddhsinh.ad@gmail.com
password : 1234

You can authenticate with this credential and get the access_token for the authorization
