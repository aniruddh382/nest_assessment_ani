import { Injectable } from '@nestjs/common'

@Injectable()
export class AppService {
  getHello(): string {
    return 'Assessment - Aniruddhsinh - NestJs REST API'
  }
}
