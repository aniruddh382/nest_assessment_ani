import { Logger, Module } from '@nestjs/common'
import { HttpModule } from '@nestjs/axios';
import { AuthModule } from './auth'
import { AppController } from './app.controller'
import { AppService } from './app.service'

import {
  UserModule,
} from './repositories'
import {
  AuthController,
  UserController,
} from './controllers'

@Module({
  imports: [
    HttpModule,
    AuthModule,
    UserModule,
  ],
  controllers: [
    AppController,
    AuthController,
    UserController,
  ],
  providers: [Logger, AppService],
})
export class AppModule {}
