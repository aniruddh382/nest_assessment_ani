import {
  Controller,
  Request,
  Get,
  UseGuards,
  Param,
  HttpStatus,
  HttpCode,
  Post,
  BadRequestException,
  Put,
  Delete,
  Query,
  Logger,
  Inject,
  LoggerService,
  Body,
  ParseIntPipe,
  DefaultValuePipe,
  NotFoundException
} from '@nestjs/common'
import {
  ApiTags,
  ApiBearerAuth,
  ApiOkResponse,
  ApiOperation,
  ApiBody,
  ApiCreatedResponse,
  ApiBadRequestResponse,
  ApiInternalServerErrorResponse,
  ApiQuery,
  ApiNotFoundResponse,
  ApiForbiddenResponse
} from '@nestjs/swagger'
import { Request as RequestBody } from 'express'
import { AuthGuard } from '@nestjs/passport'
import { isEmpty } from 'lodash'

import { ERRORS } from '../constants'
import { UserService } from '../repositories'
import { User, UserPasswords } from '../models/user'
import { AuthPayload } from '../models/auth'
import { encryptPassword }  from '../auth'

@ApiBearerAuth()
@ApiTags('Users')
@UseGuards(AuthGuard())
@Controller('/api/users')
export class UserController {
  constructor(
    private readonly userService: UserService,
    @Inject(Logger) private readonly logger: LoggerService
  ) { }

  @ApiOperation({ summary: 'Get all users' })
  @ApiOkResponse({ description: 'List of users', type: User, isArray: true })
  @ApiInternalServerErrorResponse({ description: 'Internal Server Error' })
  @ApiForbiddenResponse({ description: 'You do not have the necessary role to perform this action' })
  @ApiQuery({
    name: 'search',
    required: false,
    type: String
  })
  @ApiQuery({
    name: 'offset',
    required: false,
    type: Number
  })
  @ApiQuery({
    name: 'limit',
    required: false,
    type: Number
  })
  @Get()
  @HttpCode(HttpStatus.OK)
  async findAll(
    @Query(
      'search',
      new DefaultValuePipe('')
    ) search: string,
    @Query(
      'offset',
      new DefaultValuePipe(0),
      ParseIntPipe
    ) offset: number,
    @Query(
      'limit',
      new DefaultValuePipe(10),
      ParseIntPipe
    ) limit: number
  ): Promise<Array<User>> {
    const users = await this.userService.findAll()
    return users as Array<User>
  }

  @ApiOperation({ summary: 'Get the info of the current user' })
  @ApiOkResponse({
    type: User,
    description: 'Current user information',
  })
  @Get('me')
  @HttpCode(HttpStatus.OK)
  async getProfile(
    @Request() req: { user: AuthPayload }
  ): Promise<User> {
    return this.userService.findOne(req.user.sub)
  }

  @ApiOperation({ summary: 'Get the info of a user' })
  @ApiNotFoundResponse({ description: 'User not found' })
  @ApiForbiddenResponse({ description: 'You do not have the necessary role to perform this action' })
  @ApiOkResponse({
    type: User,
    description: 'User information'
  })
  @Get(':id')
  @HttpCode(HttpStatus.OK)
  async findOne(
    @Param('id') id: string
  ): Promise<User> {
    const user = await this.userService.findOne(id)
    if (user) {
      return user
    } else {
      throw new NotFoundException(ERRORS.USER_NOT_FOUND)
    }
  }

  @ApiOperation({ summary: 'Create a user' })
  @ApiBody({ type: User, description: 'User information' })
  @ApiCreatedResponse({ description: 'The user was created successfully' })
  @ApiBadRequestResponse({ description: 'The user could not be created' })
  @ApiForbiddenResponse({ description: 'You do not have the necessary role to perform this action' })
  @Post()
  @HttpCode(HttpStatus.CREATED)
  async addUser(
    @Body() user: User
  ): Promise<void> {
    try {
      await this.userService.addUser(user)
    } catch (error) {
      /**
       * Validate database exceptions
       */
      this.logger.error(error.message, 'ADD_USER')
      throw new BadRequestException(error.message)
    }
  }

  @ApiOperation({ summary: 'Update a user' })
  @ApiBody({ type: User, description: 'User information' })
  @ApiOkResponse({ description: 'The user was updated successfully' })
  @ApiBadRequestResponse({ description: 'The user could not be updated' })
  @ApiForbiddenResponse({ description: 'You do not have the necessary role to perform this action' })
  @Put()
  @HttpCode(HttpStatus.OK)
  async updateUser(
    @Body() user: User
  ): Promise<void> {
    try {
      await this.userService.updateUser(user)
    } catch (error) {
      /**
       * Validate database exceptions
       */
       this.logger.error(error.message, 'UPDATE_USER')
       throw new BadRequestException(error.message)
    }
  }

  @ApiOperation({ summary: 'Delete a user' })
  @ApiOkResponse({ description: 'User deleted' })
  @ApiBadRequestResponse({ description: 'The user could not be deleted' })
  @ApiForbiddenResponse({ description: 'You do not have the necessary role to perform this action' })
  @Delete(':id')
  @HttpCode(HttpStatus.OK)
  async delete(
    @Param('id') id: string
  ): Promise<void> {
    try {
      await this.userService.deleteById(id)
    } catch (error) {
      this.logger.error(error.message, 'DELETE_USER')
      throw new BadRequestException(error.message)
    }
  }

  @ApiOperation({ summary: 'Change user password' })
  @ApiBody({ type: UserPasswords, description: 'User password' })
  @ApiOkResponse({ description: 'Updated password' })
  @ApiBadRequestResponse({ description: 'The password could not be updated' })
  @Put('update-password')
  @HttpCode(HttpStatus.OK)
  async updatePassword (
    @Request() req: RequestBody<any, any, UserPasswords> & { user: AuthPayload }
  ): Promise<void> {
    const { password, repeatPassword } = req.body
    if (isEmpty(password)) {
      throw new BadRequestException('Password is required')
    } else if (password !== repeatPassword) {
      throw new BadRequestException('Passwords do not match')
    }
    try {
      const hashedPassword = await encryptPassword(password)
      await this.userService.updatePassword(req.user.sub, hashedPassword)
    } catch (error) {
      this.logger.error(error.message, 'UPDATE_PASSWORD')
      throw new BadRequestException(error.message)
    }
  }
}
