import { NestFactory } from '@nestjs/core'
import { ValidationPipe } from '@nestjs/common'
import { NestExpressApplication } from '@nestjs/platform-express'
import { json, urlencoded } from 'body-parser'
import { join , resolve } from 'path'

import { AppModule } from './app.module'
import { setupSwagger } from './swagger'
import { setupSecurity } from './security'

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule, {
    logger: ['error', 'warn', 'debug']
  })

  // BASIC CONFIGURATION
  app.useStaticAssets(resolve('./src/public'))
  app.setBaseViewsDir(resolve('./src/views'))
  app.setViewEngine('ejs')

  // PARSE REQUESTS
  app.use(urlencoded({ extended: true }))
  app.use(json({
    limit: '10mb'
  }))
  app.useGlobalPipes(
    new ValidationPipe({
      transform: true,
      whitelist: false,
      forbidNonWhitelisted: false
    })
  )

  // SECURITY
  setupSecurity(app)

  // OPEN API
  setupSwagger(app)

  // Register the proxy’s IP address (load balancer or reverse proxy)
  app.set('trust proxy', function (ip: string) {
    if (ip === '127.0.0.1') return true // trusted IPs
    else return false
  })

  await app.listen(process.env.PORT || 3000)
}
bootstrap()
