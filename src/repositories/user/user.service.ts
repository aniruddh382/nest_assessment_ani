import { Injectable, Inject } from '@nestjs/common'
import { Repository } from 'typeorm'

import { User, IUser } from '../../models/user'
import { REPOSITORIES } from '../../constants'
import { PUBLIC_TABLES } from '../../database'
import { trimStringProps } from '../utils'
import { encryptPassword } from '../../auth'

@Injectable()
export class UserService {
  constructor(
    @Inject(REPOSITORIES.USER)
    private readonly repository: Repository<User>
  ) { }

  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  preloadUser(user: User) {
    return {
      ...trimStringProps(user),
    }
  }

  async findAll(): Promise<User[]> {
    return await this.repository.query(
      `SELECT * FROM ${PUBLIC_TABLES.USER};`
    )
  }

  async findByEmail(email: string): Promise<User> {
    const rawData = await this.repository.query(
      `SELECT * FROM ${PUBLIC_TABLES.USER} WHERE email = '${email}';`)
      return rawData[0]
  }

  async findOne(id: string): Promise<User> {
    return this.repository.findOne({
      where: {
        id,
      }});
  }

  async addUser(
    user: IUser,
  ): Promise<void> {
    const newUser = this.preloadUser(user as User)
    const encryptedPassword = await encryptPassword(newUser.password)
    await this.repository.query(
      `INSERT INTO ${PUBLIC_TABLES.USER} (
        id,
        email,
        firstName,
        lastName,
        password,
        phoneNumber,
        isGraduate
      )
      VALUES ('${newUser.id}', '${newUser.email}', '${newUser.firstName}', '${newUser.lastName}', '${encryptedPassword}', '${newUser.phoneNumber}', ${newUser.isGraduate});`
      )
  }

  async updateUser(user: IUser): Promise<User> {
    const newUser = this.preloadUser(user as User)
    await this.repository.query(
      `UPDATE ${PUBLIC_TABLES.USER}
      SET email = '${newUser.email}',
        firstName = '${newUser.firstName}',
        lastName = '${newUser.lastName}',
        phoneNumber = '${newUser.phoneNumber}',
        isGraduate = ${newUser.isGraduate}
      WHERE id = ${newUser.id};`
    )
    return newUser
  }

  deleteById(id: string): Promise<void> {
    return this.repository.query(
      `DELETE FROM ${PUBLIC_TABLES.USER} WHERE id = ${id};`
    )
  }

  async updatePassword(
    id: string,
    newPassword: string
  ): Promise<void> {
    const encryptedPassword = await encryptPassword(newPassword)
    await this.repository.query(
      `UPDATE ${PUBLIC_TABLES.USER}
      SET password = "${encryptedPassword}"
      WHERE id = ${id};`
    )
  }
}