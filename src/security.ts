import { INestApplication } from '@nestjs/common'
import { DOMAIN } from './constants'

const allowedOrigins = [
  // WEBSITES
  DOMAIN,
  'http://localhost',
]

export function setupSecurity (app: INestApplication): void {
  // Enable Cross-origin resource sharing for a list of domains
  app.enableCors({
    origin: (origin, callback) => {
      if (!origin || allowedOrigins.some(o => origin.startsWith(o))) {
        callback(null, true)
      } else {
        callback(new Error('Origin not allowed by CORS'))
      }
    },
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS',
    credentials: true
  })
}