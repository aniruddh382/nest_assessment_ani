import { join } from 'path'
import { ConnectionOptions } from 'typeorm'

const config = {
  host: '10.102.32.196',
  user: 'development',
  password: 'Admin@123',
  database: 'assessment_ani',
}

const connectionOptions: ConnectionOptions = {
  type: 'mysql',
  host: config.host || 'localhost',
  port: 3306,
  username: config.user || 'development',
  password: config.password || 'Admin@123',
  database: config.database || 'assessment_ani',
  entities: [
    join(__dirname, '../models/*{.ts,.js}'),
  ],
  // We are using migrations, synchronize should be set to false.
  synchronize: false,
  dropSchema: false,
  // Run migrations automatically,
  // you can disable this if you prefer running migration manually.
  migrationsRun: true,
  logging: ['warn', 'error'],
  migrations: [
    join(__dirname, 'migrations/*{.ts,.js}')
  ],
}

export = connectionOptions
