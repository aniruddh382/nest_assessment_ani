import { INestApplication } from '@nestjs/common'
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger'

export function setupSwagger (app: INestApplication): void {
  const url = 'http'
  const options = new DocumentBuilder()
    .setTitle('Assessment NestJs - Aniruddhsinh')
    .setVersion('1.0')
    .addTag('Endpoints')
    .addBearerAuth()
    .addServer(`${url}://localhost:3000`)
    .build()
  const document = SwaggerModule.createDocument(app, options)
  SwaggerModule.setup('api', app, document)
}
