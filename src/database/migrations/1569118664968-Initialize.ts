import {
  Table,
  QueryRunner,
  MigrationInterface
} from 'typeorm'

import { encryptPassword } from '../../auth'
import { User } from '../../models/user'
import {
  PUBLIC_TABLES,
  COLUMN_TYPES,
  createAndUpdateDates,
  INDICES,
} from '../utils'

export class Initialize1569118664968 implements MigrationInterface {

  public async up(queryRunner: QueryRunner): Promise<any> {

    console.log('************** CREATE PUBLIC SCHEMA **************')
    await queryRunner.createTable(new Table({
      name: PUBLIC_TABLES.USER,
      columns: [
        { name: 'id', type: COLUMN_TYPES.VARCHAR, isPrimary: true, isGenerated: false },
        { name: 'password', type: COLUMN_TYPES.TEXT, isNullable: true },
        { name: 'firstName', type: COLUMN_TYPES.VARCHAR, length: '50' },
        { name: 'lastName', type: COLUMN_TYPES.VARCHAR, length: '50' },
        { name: 'email', type: COLUMN_TYPES.VARCHAR, length: '50' },
        { name: 'phoneNumber', type: COLUMN_TYPES.BIGINT, length: '20', isNullable: true },
        { name: 'isGraduate', type: COLUMN_TYPES.BOOLEAN, default: false },
        ...createAndUpdateDates
      ],
      indices: [
        { name: INDICES.USER_EMAIL, columnNames: ['email'], isUnique: true }
      ]
    }), true)

    console.log('************** INSERT DEFAULT DATA **************')

    // INSERT DATA
    const encryptedPassword = await encryptPassword('1234')
    const user = new User({
      id: '1234',
      password: encryptedPassword,
      email: 'aniruddhsinh.ad@gmail.com',
      firstName: 'Aniruddhsinh',
      lastName: 'Dhummad',
      phoneNumber: '7600811966',
      isGraduate: true
    })
    await queryRunner.manager.save(user)
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    console.log('************** REMOVE PUBLIC SCHEMA **************')

    await queryRunner.dropTable(PUBLIC_TABLES.USER)
  }
}
