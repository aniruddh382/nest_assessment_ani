import {
  Entity,
  Index,
  Column,
  ManyToOne,
  PrimaryColumn,
  CreateDateColumn,
  UpdateDateColumn,
  JoinTable
} from 'typeorm'
import { ApiProperty } from '@nestjs/swagger'
import { IsEmail, IsNotEmpty } from 'class-validator'

export enum UserStatus {
  Inactive = 'INACTIVE',
  Active = 'ACTIVE'
}

export interface IUser {
  id: string
  firstName: string
  lastName: string
  createDate: Date
  updateDate: Date
}

@Entity({ schema: 'public' })
export class User implements IUser {
  constructor(partial?: Partial<User>) {
    Object.assign(this, partial);
  }

  /**
   * User's document
   */
  @ApiProperty({ description: 'Document of the user' })
  @PrimaryColumn('text')
  @IsNotEmpty({
    message: 'Identification number is required'
  })
  id: string

  @ApiProperty({ description: 'First name' })
  @Column({ type: 'varchar', length: 50, name: 'firstName' })
  @IsNotEmpty({
    message: 'First name is required'
  })
  firstName: string

  @ApiProperty({ description: 'Last name' })
  @Column({ type: 'varchar', length: 50, name: 'lastName' })
  @IsNotEmpty({
    message: 'Last name is required'
  })
  lastName: string

  @ApiProperty({ description: 'Email' })
  @Column({ type: 'varchar', length: 50 })
  @Index('IDX_USER_EMAIL', { unique: true })
  @IsEmail({}, {
    message: 'The email is not valid'
  })
  @IsNotEmpty({
    message: 'Email is required'
  })
  email: string

  @ApiProperty({ description: 'Password' })
  @Column('text', { nullable: true })
  password?: string

  @ApiProperty({ description: 'Phone number' })
  @Column({ type: 'bigint', width: 12, nullable: true })
  phoneNumber?: string

  @ApiProperty({ description: 'Is Graduate' })
  @Column('boolean', { default: false })
  isGraduate?: boolean

  @CreateDateColumn({ type: 'timestamp' })
  createDate: Date

  @UpdateDateColumn({ type: 'timestamp' })
  updateDate: Date
}

export class UserPasswords {
  @ApiProperty({ description: 'User password' })
  @IsNotEmpty({
    message: 'The password is required'
  })
  password: string

  @ApiProperty({ description: 'Repeat user password' })
  @IsNotEmpty({
    message: 'Repeat password is required'
  })
  repeatPassword: string
}
